import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'
import Auth from './Auth'
import KSIAuth from './KSIAuth'

import Login from './Login'
import AdminDashboard from './components/admin/Dashboard'
import AdminAccount from './components/admin/AdminDetail'
import QuestionCategory from './components/admin/question_category/Index'
import QuestionDiscourse from './components/admin/question_discourse/Index'
import Question from './components/admin/question/Index'
import QuestionPacket from './components/admin/question_packet/Index'
import StudyProgram from './components/admin/study_program/Index'
import Participant from './components/admin/participant/Index'
import ExamSchedule from './components/admin/exam_schedule/Index'
import ExamDetail from './components/admin/exam_detail/Index'
import Report from './components/admin/reports/Index'

import ParticipantDashboard from './components/participant/Dashboard'
import ParticipantAccount from './components/participant/ParticipantDetail'
import DataCompletion from './components/participant/DataCompletion'
import ExamPage from './components/participant/Exam/Index'
import ExamSimulation from './components/participant/Simulation'

Vue.use(VueRouter)

/**
 * Roles:
 *
 * 1 => Administrator
 * 2 => Peserta
 * 3 => Staff
 */

const app_title = 'Tes Antara UAJY | '
const group = '/admin'

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/', redirect: { name: 'login' }, meta: { role: [1, 2, 3] } },
    { path: '/login', name: 'login', meta: { title: app_title + 'Login', role: [1, 2, 3] }, component: Login, props: true },

    { path: group + '/', name: 'admin-landing', meta: { title: app_title, role: [1, 3] } },
    { path: group + '/account-details', name: 'admin-account', component: AdminAccount, meta: { title: app_title + 'Detail Akun', role: [1, 3] } },
    { path: group + '/dashboard', name: 'admin-dashboard', component: AdminDashboard, meta: { title: app_title + 'Dashboard', role: [1, 3] } },
    { path: group + '/question-categories', name: 'question-category.index', component: QuestionCategory, meta: { title: app_title + 'Kategori Soal', role: [1] } },
    { path: group + '/discourses', name: 'discourse.index', component: QuestionDiscourse, meta: { title: app_title + 'Wacana Soal', role: [1] } },
    { path: group + '/questions', name: 'question.index', component: Question, meta: { title: app_title + 'Soal', role: [1] } },
    { path: group + '/packets', name: 'question-packet.index', component: QuestionPacket, meta: { title: app_title + 'Paket Soal', role: [1] } },
    { path: group + '/programs', name: 'program.index', component: StudyProgram, meta: { title: app_title + 'Program Studi', role: [1] } },
    { path: group + '/participants', name: 'participant.index', component: Participant, meta: { title: app_title + 'Peserta', role: [1, 3] } },
    { path: group + '/exam-schedules', name: 'exam-schedule.index', component: ExamSchedule, meta: { title: app_title + 'Jadwal Ujian', role: [1, 3] } },
    { path: group + '/exam-details', name: 'exam-detail.index', component: ExamDetail, meta: { title: app_title + 'Detail Ujian', role: [1, 3] } },
    { path: group + '/reports', name: 'report.index', component: Report, meta: { title: app_title + 'Laporan', role: [1] } },

    { path: '/dashboard', name: 'participant-dashboard', component: ParticipantDashboard, meta: { title: app_title + 'Dashboard', role: [2] } },
    { path: '/account-details', name: 'participant-account', component: ParticipantAccount, meta: { title: app_title + 'Detail Akun', role: [2] } },
    { path: '/data-completion', name: 'data-completion', component: DataCompletion, meta: { title: app_title + 'Kelengkapan Data', role: [2] } },
    { path: '/exam', name: 'exam.index', component: ExamPage, meta: { title: app_title + 'Ujian', role: [2] } },
    { path: '/simulation', name: 'exam.simulation', component: ExamSimulation, meta: { title: app_title + 'Simulasi Ujian', role: [2] } },
  ]
})
  
router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  Auth.refetchToken()
  KSIAuth.refetchToken()

  let user = store.getters['loggedUser/getData']
  let isLoggedIn = store.getters['loggedUser/getLogin']

  if (user && isLoggedIn) {
    to.meta.role.indexOf(user.role.id) != -1 ? next() : next(false)
  } else {
    to.name == 'login' ? next() : next({'name': 'login'})
  }
})

export default router
