import axios from 'axios';

const KSIAPI = axios.create()

export default {
  init() {
    KSIAPI.defaults.baseURL = 'https://api-mission.uajy.ac.id/api'
  },

  setToken(token) {    
    KSIAPI.interceptors.request.use((config) => {
      const newConfig = config
      newConfig.headers.Authorization = `Bearer ${token}`

      return newConfig
    })
  },

  post(url, data, successCallback, errorCallback) {
    return KSIAPI.request({
      url,
      data,
      method: 'post',
    })
    .then(successCallback)
    .catch(errorCallback);
  },

  get(url, successCallback, errorCallback) {
    return KSIAPI.request({
      url,
      method: 'get',
    })
    .then(successCallback)
    .catch(errorCallback);
  },

  put(url, data, successCallback, errorCallback) {
    return KSIAPI.request({
      url,
      data,
      method: 'put',
    })
    .then(successCallback)
    .catch(errorCallback);
  },

  delete(url, successCallback, errorCallback) {
    return KSIAPI.request({
      url,
      method: 'delete',
    })
    .then(successCallback)
    .catch(errorCallback);
  },
};
