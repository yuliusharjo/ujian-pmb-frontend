import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import http from './http'
import KSIHttp from './KSIHttp'
import VeeValidate from 'vee-validate'

Vue.use(VeeValidate)

Vue.config.productionTip = false

Vue.prototype.$imageURL = 
  process.env.NODE_ENV == 'production' 
  //? 'https://api-ujianpmb.uajy.ac.id/pmbapi/images/' 
   ? 'https://ujian-pmb.uajy.ac.id/pmbapi/images/' 
  : 'http://localhost:8000/images/'

http.init()
KSIHttp.init()

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
