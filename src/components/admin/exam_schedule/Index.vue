<template>
  <v-layout row wrap>
    <v-breadcrumbs :items="breadrumbItems" divider=">" class="pt-2"></v-breadcrumbs>
    <v-flex xs12>      
      <v-card>
        <v-card-title>
          <span class="title mr-2">Jadwal dan Pengaturan Waktu Ujian</span>
          <schedule-form            
            v-on:closeEditDialog="closeEditDialog"  
          ></schedule-form>
          <v-spacer></v-spacer>
          <v-text-field
            append-icon="search"
            label="Cari/Filter"
            single-line
            hide-details
            v-model="search"
            clearable
          ></v-text-field>
        </v-card-title>
        <data-table
          :search="search"
          :headers="headers"
          :progress="progress"
          :edit="true"
          :destroy="true"
          :items="scheduleData"
          v-on:showEditDialog="showEditDialog"
          v-on:destroyHandler="destroyScheduleHandler"
        ></data-table>
      </v-card>
    </v-flex>
  </v-layout>
</template>

<script>
  /* eslint-disable */
  import { mapGetters, mapActions, mapMutations } from 'vuex';

	import DataTable from '../../DataTable';
  import ScheduleForm from './Form';

	export default {
    name: 'ExamSchedule',

    components: {
      DataTable,
      ScheduleForm,
    },

    data: () => ({
      breadrumbItems: [
        {
          text: 'Dashboard',
          disabled: false,
          href: 'dashboard'
        },
        {
          text: 'Jadwal dan Pengaturan Waktu Ujian',
          disabled: true
        },
      ],
      headers: [
        { text: 'Id', value: 'id', align: 'right' },
        { text: 'Waktu Mulai', value: 'start_time' },
        { text: 'Waktu Selesai', value: 'end_time' },
        { text: 'Cut-off (Dalam Menit)', value: 'cut_off', align: 'right' },
        { text: 'Status Jadwal', value: 'status' },
        { text: 'Keterangan', value: 'note' },
        { text: 'Aksi', value: 'actions', sortable: false },
      ],      
      search: '',
      progress: true
    }),

    computed: {
      ...mapGetters({
        scheduleData: 'examSchedule/getData',
        scheduleActive: 'examSchedule/getActive',
      }),
    },

    methods: {
      ...mapActions({
        fetchSchedules: 'examSchedule/fetchAll', 
        destroySchedule: 'examSchedule/destroy',   
      }),

      ...mapMutations({
        setActiveSchedule: 'examSchedule/setActive'
      }),

      fetchSchedulesHandler() {
        this.fetchSchedules()
          .then((res) => {
            this.progress = false
          })
          .catch(err => {
            console.log(err);
          });
      },

      showEditDialog(item) {
        this.setActiveSchedule(item);
      },

      closeEditDialog() {
        this.setActiveSchedule({});
      },

      destroyScheduleHandler(id) {
        this.destroySchedule(id)
          .catch(err => {
            console.log(err);
          });
      },
    },

    mounted() {
      this.fetchSchedulesHandler();
    }
  }
</script>