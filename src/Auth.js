import jwt from 'jsonwebtoken'
import Cookies from 'js-cookie'
import http from './http'
import store from './store'

export default {
  authenticate(username, password) {
    return new Promise((resolve, reject) => {
      const payload = {
        username,
        password,
      };

      const successCallback = (res) => {        
        if (res.status === 200) {
          this.storeJwt(res.data.access_token);
          store.commit('loggedUser/setStatus', res.data.status);
          this.refetchToken();          
          store.commit('loggedUser/setLogin', true)
        }
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign(err);
        reject(errData);
      };

      http.post('login', payload, successCallback, errorCallback);
    });
  },

  storeJwt(token) {
    let expires = 1;
    Cookies.set('jwt', token, {
      expires: expires,
    });
  },

  getJwt() {
    return Cookies.get('jwt');
  },

  clearJwt() {
    Cookies.remove('jwt');
    this.refetchToken()
  },

  decodeJwt(token) {
    const decodedToken = jwt.decode(token, { complete: true });
    
    return decodedToken ? decodedToken.payload.user : null;
  },

  refetchToken() {
    const token = this.getJwt()
    const user = this.decodeJwt(token)

    if (user) {      
      store.commit('loggedUser/setSource', user)
      http.setToken(token)
    }    
  },
};
