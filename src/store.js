import Vue from 'vue'
import Vuex from 'vuex'

import questionCategory from './stores/questionCategory'
import questionDiscourse from './stores/questionDiscourse'
import question from './stores/question'
import questionPacket from './stores/questionPacket'
import answerChoice from './stores/answerChoice'
import image from './stores/image'
import studyProgram from './stores/studyProgram'
import user from './stores/user'
import participant from './stores/participant'
import examSchedule from './stores/examSchedule'
import examDetail from './stores/examDetail'
import examAnswer from './stores/examAnswer'
import loggedUser from './stores/loggedUser'
import report from './stores/report'

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    questionCategory,
    questionDiscourse,
    question,
    questionPacket,
    answerChoice,
    image,
    studyProgram,
    user,
    participant,
    examSchedule,
    examDetail,
    examAnswer,
    loggedUser,
    report
  },
});

export default store
