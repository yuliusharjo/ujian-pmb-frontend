import jwt from 'jsonwebtoken'
import Cookies from 'js-cookie'
import KSIHttp from './KSIHttp'
// import axios from 'axios'
// import store from './store'

export default {
  authenticate(username, password) {
    return new Promise((resolve, reject) => {
      const payload = {
        username,
        password,
      };

      const successCallback = (res) => {        
        if (res.status === 200) {
          this.storeJwt(res.data.data.token);
          this.refetchToken();          
        }
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign(err);
        reject(errData);
      };

      KSIHttp.post('auth/login', payload, successCallback, errorCallback);
    });

  },

  storeJwt(token) {
    let expires = 1;
    Cookies.set('ksi-jwt', token, {
      expires: expires,
    });
  },

  getJwt() {
    return Cookies.get('ksi-jwt');
  },

  clearJwt() {
    Cookies.remove('ksi-jwt');
    this.refetchToken()
  },

  decodeJwt(token) {
    const decodedToken = jwt.decode(token, { complete: true });
    
    return decodedToken ? decodedToken.payload : null;
  },

  refetchToken() {
    const token = this.getJwt()
    const user = this.decodeJwt(token)
    // console.log(user)

    if (user) {      
      KSIHttp.setToken(token)
    }
    // KSIHttp.setToken(token)
    // console.log(token)
    // store.commit('loggedUser/setKSIToken', token)   
  },
};
