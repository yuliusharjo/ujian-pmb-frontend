import http from '../http'
import Cookies from 'js-cookie'

export default {
  namespaced: true,

  state: {
    data: null,
    participant: null,
    status: 0,
    isLoggedIn: !!Cookies.get('jwt'),
  },

  getters: {
    getData: state => state.data,
    getParticipant: state => state.participant,
    getLogin: state => state.isLoggedIn,
    getUserStatus: state => state.status
  },
  
  mutations: {
    setSource(state, source) {
      state.data = source
    },

    setParticipant(state, source) {
      state.participant = source
    },

    setLogin(state, source) {
      state.isLoggedIn = source
    },

    setStatus(state, source) {
      state.status = source
    }
  },

  actions: {
    update(context, payload) {
      return new Promise((resolve, reject) => {  
        const successCallback = (res) => {
          if (res.status === 200) {
            resolve(res)
          }
        }

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response)
          reject(errData)
        }

        http.put(`users/${payload.id}`, payload, successCallback, errorCallback)
      })
    },

    fetchOne(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setParticipant', res.data)
            resolve(res.data)
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`users/${id}`, successCallback, errorCallback);
      });
    }
  }
};