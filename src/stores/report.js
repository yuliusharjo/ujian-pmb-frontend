/* eslint-disable */
import http from '../http';

export default {
  namespaced: true,
  
  state: {
    data: [],
  },

  getters: {
    getData: state => state.data,
  },

  mutations: {
    setData(state, source) {
      state.data = source;
    },
  },

  actions: {
    getRegisteredUsersReport(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`reports/registered-users?start-date=${payload.start_date}&end-date=${payload.end_date}`, successCallback, errorCallback);
      });
    },

    getPassedUsersReport(context, payload) {
        return new Promise((resolve, reject) => {
          const successCallback = res => {
            if (res.status === 200) {
              resolve(res.data);
            }
          };
  
          const errorCallback = err => {
            const errData = Object.assign({}, err);
            reject(errData.response);
          };
  
          http.get(`reports/passed-users?start-date=${payload.start_date}&end-date=${payload.end_date}`, successCallback, errorCallback);
        });
      },
  },
};