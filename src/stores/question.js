/* eslint-disable */
import http from '../http'

export default {
  namespaced: true,
  
  state: {
    data: [],
    active: {},
    detailed: {},
    answerChoices: null,
    newChoices: []
  },

  getters: {
    getData: state => state.data,
    getActive: state => state.active,
    getDetailed: state => state.detailed,
    getAnswerChoices: state => state.answerChoices,
    getNewChoices: state => state.newChoices
  },

  mutations: {
    setData(state, source) {
      state.data = source
    },

    setActive(state, source) {
      state.active = source
    },

    setDetailed(state, source) {
      state.detailed = source
    },

    setAnswerChoices(state, source) {
      state.answerChoices = source
    },

    setNewChoices(state, source) {
      state.newChoices = source
    }
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setData', res.data)
            resolve(res.data)
          }
        }

        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }

        http.get('questions', successCallback, errorCallback)
      })
    },

    fetchAllBylevel(context, level) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setData', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`questions?level=${level}`, successCallback, errorCallback);
      });
    },

    fetchOne(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        }

        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }

        http.get(`questions/${id}`, successCallback, errorCallback)
      })
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        }

        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }

        http.post('questions', payload, successCallback, errorCallback)
      })
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              })
          }
        }

        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }

        http.put(`questions/${payload.id}`, payload, successCallback, errorCallback)
      })
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }

        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }

        http.delete(`questions/${id}`, successCallback, errorCallback)
      })
    },

    fetchAnswerChoices(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        }

        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        };

        http.get(`questions/${id}/answer-choices`, successCallback, errorCallback)
      })
    },

    storeAnswerChoices(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };
  
        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };
  
        http.put(`questions/${payload.id}/answer-choices`, payload, successCallback, errorCallback);
      });
    },

    // fetchOneForExam(context, id) {
    //   return new Promise((resolve, reject) => {
    //     const successCallback = res => {
    //       if (res.status === 200) {
    //         resolve(res.data)
    //       }
    //     }

    //     const errorCallback = err => {
    //       const errData = Object.assign({}, err)
    //       reject(errData.response)
    //     }

    //     http.get(`questions/${id}?on-exam=true`, successCallback, errorCallback)
    //   })
    // },
  },
}