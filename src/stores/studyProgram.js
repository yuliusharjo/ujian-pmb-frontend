/* eslint-disable */
import http from '../http';
import KSIHttp from '../KSIHttp'

export default {
  namespaced: true,
  
  state: {
    data: [],
    active: {},
    KSIData: []
  },

  getters: {
    getData: state => state.data,
    getActive: state => state.active,
    getKSIData: state => state.KSIData
  },

  mutations: {
    setData(state, source) {
      state.data = source;
    },

    setActive(state, source) {
      state.active = source;
    },

    setKSIData(state, source) {
      state.KSIData = source
    }
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setData', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('programs', successCallback, errorCallback);
      });
    },

    fetchOne(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`programs/${id}`, successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post('programs', payload, successCallback, errorCallback);
      });
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.put(`programs/${payload.id}`, payload, successCallback, errorCallback);
      });
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.delete(`programs/${id}`, successCallback, errorCallback);
      });
    },

    fetchAllByLevel(context, level) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setData', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`programs?level=${level}`, successCallback, errorCallback);
      });
    },

    resolveQuestionCategory(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };
  
        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };
  
        http.post(`programs/${payload.id}/categories`, payload, successCallback, errorCallback);
      });
    },

    fetchFromKSI(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setKSIData', res.data.data);
            resolve(res.data.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        KSIHttp.get('prodi', successCallback, errorCallback);
      });
    }

    // removeQuestionCategory(context, payload) {
    //   return new Promise((resolve, reject) => {
    //     const successCallback = res => {
    //       if (res.status === 200) {
    //         resolve(res.data)
    //         console.log('tes')
    //       }
    //     };
  
    //     const errorCallback = err => {
    //       const errData = Object.assign({}, err);
    //       reject(errData.response);
    //     };
  
    //     http.delete(`programs/${payload.id}/categories`, payload, successCallback, errorCallback);
    //   });
    // },
  },
};