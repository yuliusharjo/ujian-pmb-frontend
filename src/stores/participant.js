/* eslint-disable */
import http from '../http';
import KSIHttp from '../KSIHttp'

export default {
  namespaced: true,
  
  state: {
    data: [],
    active: {},
    detail: {},
    KSIData: []
  },

  getters: {
    getData: state => state.data,
    getActive: state => state.active,
    getDetail: state => state.detail,
    getKSIData: state => state.KSIData
  },

  mutations: {
    setData(state, source) {
      state.data = source
    },

    setActive(state, source) {
      state.active = source
    },

    setDetail(state, source) {
      state.detail = source
    },

    setKSIData(state, source) {
      state.KSIData = source
    }
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setData', res.data);
            resolve(res.data)
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response)
        };

        http.get('participants', successCallback, errorCallback);
      });
    },

    fetchOne(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`participants/${id}`, successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response)
        };

        http.post('participants', payload, successCallback, errorCallback);
      });
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response)
        };

        http.put(`participants/${payload.id}`, payload, successCallback, errorCallback);
      });
    },

    updateLogout(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response)
        };

        http.put(`participantsLogout/${payload.id}`, payload, successCallback, errorCallback);
      });
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        };

        http.delete(`participants/${id}`, successCallback, errorCallback);
      });
    },

    addStudyPrograms(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response)
        };

        http.put(`participants/${payload.id}/programs`, payload, successCallback, errorCallback);
      });
    },

    fetchAllByLevel(context, level) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setData', res.data);
            resolve(res.data)
          }
        };
  
        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response)
        };
  
        http.get(`participants?level=${level}&exam-detail=true`, successCallback, errorCallback);
      });
    },

    fetchFromKSI(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setKSIData', res.data.data);
            resolve(res.data.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        KSIHttp.get('mahasiswa', successCallback, errorCallback);
      });
    },

    storeOne(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response)
        };

        http.post('participants?store-one=true', payload, successCallback, errorCallback);
      });
    },
  },
};