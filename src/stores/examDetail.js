/* eslint-disable */
import http from '../http';
import KSIHttp from '../KSIHttp'

export default {
  namespaced: true,
  
  state: {
    data: [],
    active: {},
    detail: {}
  },

  getters: {
    getData: state => state.data,
    getActive: state => state.active,
    getDetail: state => state.detail
  },

  mutations: {
    setData(state, source) {
      state.data = source;
    },

    setActive(state, source) {
      state.active = source;
    },

    setDetail(state, source) {
      state.detail = source
    }
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setData', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('exam-details', successCallback, errorCallback);
      });
    },

    fetchScores(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`exam-details/scores/${id}`, successCallback, errorCallback);
      });
    },

    fetchOne(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`exam-details/${id}`, successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post('exam-details', payload, successCallback, errorCallback);
      });
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.put(`exam-details/${payload.id}`, payload, successCallback, errorCallback);
      });
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.delete(`exam-details/${id}`, successCallback, errorCallback);
      });
    },

    showAnswers(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`exam-details/${id}/exam-answers`, successCallback, errorCallback);
      });
    },

    showSimulationAnswers(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`exam-details/${id}/exam-answers?simulation=true`, successCallback, errorCallback);
      });
    },

    // showSummaries(context, id) {
    //   return new Promise((resolve, reject) => {
    //     const successCallback = res => {
    //       if (res.status === 200) {
    //         resolve(res.data);
    //       }
    //     };

    //     const errorCallback = err => {
    //       const errData = Object.assign({}, err);
    //       reject(errData.response);
    //     };

    //     http.get(`exam-details/${id}/exam-answers?summary=true`, successCallback, errorCallback);
    //   });
    // },

    showSummaries(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`exam-details/${id}/answer-summaries`, successCallback, errorCallback);
      });
    },

    finishExam(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data) 
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.put(`exam-details/${payload.id}?finish-exam=true`, payload, successCallback, errorCallback);
      });
    },

    storeEmptyAnswers(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };
  
        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };
  
        http.put(`exam-details/${payload.id}/exam-answers`, payload, successCallback, errorCallback);
      });
    },

    storeSummaries(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };
  
        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };
  
        http.post(`exam-details/${id}/answer-summaries`, successCallback, errorCallback);
      });
    },

    deleteSummaries(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };
  
        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };
  
        http.delete(`exam-details/${id}/answer-summaries`, successCallback, errorCallback);
      });
    },

    deleteAnswers(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };
  
        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };
  
        http.delete(`exam-details/${id}/exam-answers`, successCallback, errorCallback);
      });
    },

    deleteSimulationAnswers(context, id) { //NEW
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };
  
        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };
  
        http.delete(`exam-details/${id}/exam-answers?simulation=true`, successCallback, errorCallback);
      });
    },

    uploadToKSI(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };
  
        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };
  
        KSIHttp.put('nilai/update', payload, successCallback, errorCallback);
      });
    },
  },
};