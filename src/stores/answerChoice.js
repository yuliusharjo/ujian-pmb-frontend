/* eslint-disable */
import http from '../http';

export default {
  namespaced: true,
  
  state: {
    data: [],
    active: {},
    newData: [{ correct_choice: null }, { correct_choice: null }, { correct_choice: null }, { correct_choice: null }, { correct_choice: null }]
  },

  getters: {
    getData: state => state.data,
    getActive: state => state.active,
    getNewData: state => state.newData
  },

  mutations: {
    setData(state, source) {
      state.data = source
    },

    setActive(state, source) {
      state.active = source
    },

    pushNewData(state, source) {
      state.newData[source.index - 1] = source
    },
  },

  actions: {
    fetchAll(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setData', res.data);
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get('answer-choices', successCallback, errorCallback);
      });
    },

    fetchOne(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data);
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.get(`answer-choices/${id}`, successCallback, errorCallback);
      });
    },

    store(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
            context.dispatch('fetchAll')
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.post('answer-choices', payload, successCallback, errorCallback);
      });
    },

    update(context, payload) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.put(`answer-choices/${payload.id}`, payload, successCallback, errorCallback);
      });
    },

    destroy(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('fetchAll')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err);
              });
          }
        };

        const errorCallback = err => {
          const errData = Object.assign({}, err);
          reject(errData.response);
        };

        http.delete(`answer-choices/${id}`, successCallback, errorCallback);
      });
    }
  },
};