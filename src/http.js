import axios from 'axios';

const API = axios.create()

export default {
  baseURL() { return API.defaults.baseURL },
  init() {
    API.defaults.baseURL =
      process.env.NODE_ENV == 'production'
        //? 'https://api-ujianpmb.uajy.ac.id/pmbapi/images/'
        ? 'https://ujian-pmb.uajy.ac.id/pmbapi/api'
        : 'http://localhost:8000/api'
  },

  setToken(token) {
    API.interceptors.request.use((config) => {
      var newConfig = config
      newConfig.headers.Authorization = `Bearer ${token}`

      return newConfig
    })
  },

  post(url, data, successCallback, errorCallback) {
    return API.request({
      url,
      data,
      method: 'post',
    })
      .then(successCallback)
      .catch(errorCallback);
  },

  download(url, successCallback, errorCallback) {
    return API.request({
      url: url,
      method: 'GET',
      responseType: 'blob', // important
    })
      .then(successCallback)
      .catch(errorCallback);
  },

  get(url, successCallback, errorCallback) {
    return API.request({
      url,
      method: 'get',
    })
      .then(successCallback)
      .catch(errorCallback);
  },

  put(url, data, successCallback, errorCallback) {
    return API.request({
      url,
      data,
      method: 'put',
    })
      .then(successCallback)
      .catch(errorCallback);
  },

  delete(url, successCallback, errorCallback) {
    return API.request({
      url,
      method: 'delete',
    })
      .then(successCallback)
      .catch(errorCallback);
  },
};
